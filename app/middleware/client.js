import request from 'superagent';
import Promise, { using } from 'bluebird';
import _ from 'lodash';
import config from 'config';


var clientService = {},
  APIUrl = config.API_BASE_URL + '/clients/';


// This responds a GET request for the client listing.
clientService.list = function(success, error){
  request
    .get(APIUrl)
    .set('Accept', 'application/json')
    .end(function(err, res){
      if(err){
        error(err);
      } else {
        success(res);
      }
    });
};

// This responds a POST request for create new client
clientService.post = function(data, success, error){
  request
    .post(APIUrl)
    .send(data)
    .set('Accept', 'application/json')
    .end(function(err, res){
      if(err){
        error(err);
      } else {
        success(res);
      }
    });
};

// This responds a PUT request for update client
clientService.put = function(data, success, error){
  request
    .put(APIUrl+data.id)
    .send(data)
    .set('Accept', 'application/json')
    .end(function(err, res){
      if(err){
        error(err);
      } else {
        success(res);
      }
    });
};



// This responds a DELETE request for a client.
clientService.del = function(id, success, error){
  request
    .del(APIUrl + id)
    .set('Accept', 'application/json')
    .end(function(err, res){
      if(err){
        error(err);
      } else {
        success(res);
      }
    });
};

// This responds a GET request for get a client
clientService.get = function(id, success, error){
  request
    .get(APIUrl + id)
    .set('Accept', 'application/json')
    .end(function(err, res){
      if(err){
        error(err);
      } else {
        success(res);
      }
    });
};

module.exports = clientService;