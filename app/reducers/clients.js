import * as ActionType from 'actions/clients'
import Immutable from 'immutable'

let defaultState = Immutable.fromJS([])
function clientsReducer (state = defaultState, action) {
  switch(action.type) {
    case ActionType.LOADED_CLIENTS:
      return Immutable.fromJS(action.response)
      break
    case ActionType.UPDATED_CLIENTS:
      return state.merge(action.response)
    case ActionType.GET_CLIENT:
      return state.merge({ clientDetail: action.response })  
    default:
      return state
  }
}

export default clientsReducer
