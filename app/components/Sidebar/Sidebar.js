import React, {Component} from 'react';
import { Link } from 'react-router';

import root from 'window-or-global';
import appRoutes from 'routes/app.jsx';

class Sidebar extends Component{
    constructor(props){
        super(props);
        this.state = {
            width: root.innerWidth
        }
    }
    activeRoute(routeName) {
        return this.props.location.pathname.indexOf(routeName) > -1 ? 'active' : '';
    }
    updateDimensions(){
        this.setState({width:root.innerWidth});
    }
    componentDidMount() {
        this.updateDimensions();
        root.addEventListener("resize", this.updateDimensions.bind(this));
    }
    render(){
        const sidebarBackground = {
            backgroundImage: 'url(../../assets/img/sidebar-3.jpg)'
        };
        return (
            <div id="sidebar" className="sidebar" data-color="black" data-image={'../../assets/img/sidebar-3.jpg'}>
                <div className="sidebar-background" style={sidebarBackground}></div>
                <div className="logo">
                    <a href="https://www.cynopsis-client.com" className="simple-text logo-mini">
                        <div className="logo-img">
                            <img src={'../../assets/img/reactlogo.png'} alt="logo_image" className="img-fluid"/>
                        </div>
                    </a>
                    <a href="https://www.cynopsis-client.com" className="simple-text logo-normal">
                        Cynopsis 
                    </a>
                </div>
                <div className="sidebar-wrapper"> 
                    <ul className="nav">
                        <li className="nav-item active">
                            <a href="/clients" className="nav-link">
                                <i className="pe-7s-user"></i>
                                <p>Clients</p>
                            </a>
                        </li>  
                    </ul>
                </div>
            </div>
        );
    }
}

export default Sidebar;
