import React, { Component } from 'react';
import {
    Collapse,
    Navbar,
    NavbarToggler,
    NavbarBrand,
    Nav,
    NavItem,
    NavLink,
    UncontrolledDropdown,
    DropdownToggle,
    DropdownMenu,
    DropdownItem } from 'reactstrap';
import config from 'config'

class Header extends Component{
    constructor(props){
        super(props);
        this.mobileSidebarToggle = this.mobileSidebarToggle.bind(this);
        this.state = {
            sidebarExists: false
        };
    }

    getBrand(){
        var name = "Cynopsis";
        var routeNames = config.ROUTENAMES;
        routeNames.map((prop,key) => {
            if(prop.path === this.props.location.pathname){
                name = prop.name;
            }
            return name;
        })
        return name;
    }
    
    // Toggle for smale device
    mobileSidebarToggle(e){
        if(this.state.sidebarExists === false){
          this.setState({
              sidebarExists : true
          });
        }
        e.preventDefault();
        document.documentElement.classList.toggle('nav-open');
        var node = document.createElement('div');
        node.id = 'bodyClick';
        node.onclick = function(){
            this.parentElement.removeChild(this);
            document.documentElement.classList.toggle('nav-open');
        };
        document.body.appendChild(node);
    }
    
    render(){
        return (
        <Navbar className="navbar-default height60" color="faded" light expand="md">
            <NavbarBrand href="/">{this.getBrand()}</NavbarBrand>
            <NavbarToggler onClick={this.mobileSidebarToggle} />
            <Collapse  navbar>
                <Nav className="ml-auto" navbar>
                    <NavItem>
                        <NavLink href="https://reactstrap.github.io/components">Components</NavLink>
                    </NavItem>
                    <NavItem>
                        <NavLink href="https://bitbucket.org/Duynguyen2301/cynopsis-code-test"> Repository </NavLink>
                    </NavItem>
                </Nav>
            </Collapse>
        </Navbar>            
        );
    }
}

export default Header;
