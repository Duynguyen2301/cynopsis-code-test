import React from 'react';
import { Pagination, PaginationItem, PaginationLink } from 'reactstrap';

export default class PaginationComponent extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      data: ['a','b','c','d','e','f','g','h','i','j','k'],
      currentPage: 1,
      pageSize: 3
    };
    this.handleClick = this.handleClick.bind(this);
  }

  handleClick(event) {
    this.setState({
      currentPage: Number(event.target.id)
    });
  }

  render() {
    const { datas, currentPage, pageSize } = this.state;

    // Logic for displaying data
    const indexOfLastData = currentPage * pageSize;
    const indexOfFirstData = indexOfLastData - pageSize;
    const currentDatas = datas.slice(indexOfFirstData, indexOfLastData);

    const renderDatas = currentDatas.map((data, index) => {
      return <li key={index}>{data}</li>;
    });

    // Logic for displaying page numbers
    const pageNumbers = [];
    for (let i = 1; i <= Math.ceil(datas.length / pageSize); i++) {
      pageNumbers.push(i);
    }

    const renderPageNumbers = pageNumbers.map(number => {
      return (
        <li
          key={number}
          id={number}
          onClick={this.handleClick}
        >
          {number}
        </li>
      );
    });

    return (
      <div>
        <ul>
          {renderDatas}
        </ul>
        <ul id="page-numbers">
          {renderPageNumbers}
        </ul>
      </div>
    );
  }
  // render() {
  //   return (
  //     <Pagination>
  //       <PaginationItem>
  //         <PaginationLink previous href="#" />
  //       </PaginationItem>
  //       <PaginationItem>
  //         <PaginationLink href="#">
  //           1
  //         </PaginationLink>
  //       </PaginationItem>
  //       <PaginationItem>
  //         <PaginationLink href="#">
  //           2
  //         </PaginationLink>
  //       </PaginationItem>
  //       <PaginationItem>
  //         <PaginationLink href="#">
  //           3
  //         </PaginationLink>
  //       </PaginationItem>
  //       <PaginationItem>
  //         <PaginationLink href="#">
  //           4
  //         </PaginationLink>
  //       </PaginationItem>
  //       <PaginationItem>
  //         <PaginationLink href="#">
  //           5
  //         </PaginationLink>
  //       </PaginationItem>
  //       <PaginationItem>
  //         <PaginationLink next href="#" />
  //       </PaginationItem>
  //     </Pagination>
  //   );
  // }
}