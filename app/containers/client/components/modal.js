import React, { Component } from 'react'
import { connect } from 'react-redux'
import {getClient} from 'actions/clients'

class ClientModal extends Component {
  static fetchData({ store, params, history }) {
    let { id } = params
    return store.dispatch(loadQuestionDetail({ id, history }))
  }
  componentDidMount() {
    let { id } = this.props.params
    this.props.loadQuestionDetail({ id, history: browserHistory })
  }
  render() {
    let { question } = this.props
    return (
      <div>
        <Modal isOpen={this.state.modal} toggle={this.toggle} className={this.props.className} backdrop={this.state.backdrop}>
        <ModalHeader toggle={this.toggle}>{this.state.modalLabel}</ModalHeader>
        <ModalBody>
            {this.state.data}
            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
            {this.state.isView}
        </ModalBody>
        <ModalFooter>
            <Button color="primary" onClick={this.toggle}>Do Something</Button>{' '}
            <Button color="secondary" onClick={this.toggle}>Cancel</Button>
        </ModalFooter>
        </Modal>
      </div>
    )
  }
}

function mapStateToProps (state) {
  return { question: state.questionDetail }
}


export { Question }
export default connect(mapStateToProps, { getClient })(ClientModal)
