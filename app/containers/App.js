import React, { Component } from 'react'
import {
  Route,
  Switch,
  Redirect
} from 'react-router-dom';
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import Helmet from 'react-helmet'
import NotificationSystem from 'react-notification-system';
import Header from 'components/Header/Header';
import Footer from 'components/Footer/Footer';
import Sidebar from 'components/Sidebar/Sidebar';
import {style} from "variables/Variables.jsx";
import appRoutes from "routes/app.jsx";


class App extends Component {
  constructor(props){
    super(props);
    this.componentDidMount = this.componentDidMount.bind(this);
    this.handleNotificationClick = this.handleNotificationClick.bind(this);
    
    this.state = {
        _notificationSystem: null,
        loading: true
    };
  }
  handleNotificationClick(position){
    var color = Math.floor((Math.random() * 4) + 1);
    var level ='success';    
    this.state._notificationSystem.addNotification({
        title: (<span data-notify="icon" className="pe-7s-gift"></span>),
        message: (
            <div>
                Welcome to <b> Cynopsis Dashboard </b>                   
            </div>
        ),
        level: level,
        position: position,
        autoDismiss: 10,
    });
  }
  componentDidMount(){
    this.setState({
        _notificationSystem: this.refs.notificationSystem,
        loading: false })
    let _notificationSystem = this.refs.notificationSystem;
    let color = Math.floor((Math.random() * 4) + 1)
    let level ='success';
    _notificationSystem.addNotification({
        title: (<span data-notify="icon" className="pe-7s-gift"></span>),
        message: (
            <div>
                Welcome to <b>Cynopsis Dashboard</b>
            </div>
        ),
        level: level,
        position: "tr",
        autoDismiss: 10,
    });
    this.setState({ loading: false })
  }

  

  componentDidUpdate(e){
    if(window.innerWidth < 993 && e.history.location.pathname !== e.location.pathname && document.documentElement.className.indexOf('nav-open') !== -1){
        document.documentElement.classList.toggle('nav-open');
    }
  }

  render() {
    return (
        <div className="wrapper">
        <Helmet
          defaultTitle="Code Test"
          titleTemplate="%s - Code Test"
          meta={[
            {"name": "description", "content": "Redux + React-router + Express "},
          ]}
          htmlAttributes={{"lang": "en"}}
        />
        <NotificationSystem ref="notificationSystem" style={style}/>
        {this.state.loading && 
            <div className="spinner">
                <div className="double-bounce1"></div>
                <div className="double-bounce2"></div>
            </div>
        }
        {!this.state.loading && 
            <div>
                <Sidebar {...this.props} />
                <div id="main-panel" className="main-panel">
                    <Header {...this.props}/>
                    <div className="content">
                        { this.props.children }
                    </div>
                </div>
                <Footer />       
            </div> 
        }
        </div>
    )
  }
}

function mapStateToProps(state) {
  return {}
}

export default connect(mapStateToProps)(App)
