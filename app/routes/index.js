import React from 'react'
import { Provider } from 'react-redux'
import { Router, Route, IndexRoute } from 'react-router'
import configureStore from 'store/configureStore'
import Clients from 'containers/client/clients'
import App from 'containers/App'

export default function(history) {
  return (
    <Router history={history}>
      <Route path="/" component={App}>
        <Route path="clients" component={Clients} />
        <Route path="clients/:id" component={Clients} />
        {/* <IndexRoute component={App} /> */}
      </Route>
    </Router>
  )
}
