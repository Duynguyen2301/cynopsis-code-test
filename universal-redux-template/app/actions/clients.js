import { CALL_API, CHAIN_API } from 'middleware/api'

export const LOADED_CLIENTS = Symbol('LOADED_CLIENTS')
export function loadClients() {
  return {
    [CALL_API]: {
      method: 'get',
      path: '/clients',
      successType: LOADED_CLIENTS
    }
  }
}
