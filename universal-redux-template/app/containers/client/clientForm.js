import React, { Component } from 'react';
import ReactDOM, {Input} from 'react-dom';
import { connect } from 'react-redux';
import { Link } from 'react-router';
import clientService from '../../middleware/client';
import tv4 from 'tv4';
import {loadClients} from 'actions/clients'
import { Table } from 'reactstrap';

/**
  tv4 custom message
*/

// tv4.setErrorReporter(function (error, data, schema) {
//   if(schema.errorMessage && schema.errorMessage[error.code]){
//       return schema.errorMessage[error.code];
//   }
//   return;
// });
class ClientForm extends Component {
    static fetchData({ store }) {
        return store.dispatch(loadClients())
      }
    componentDidMount() {
    this.props.loadClients()
    }
    render() {
        return (
            <div>
            <Table>
                <thead>
                <tr>
                    <th>#</th>
                    <th>Client Name</th>
                    <th>Stage</th>
                    <th>Created Date</th>
                    <th>Updated Date</th>
                </tr>
                </thead>
                <tbody>
                    {
                    this.props.clients.map((q,index)=> {
                        let id = q.get('id')
                        return (
                        <tr key={id}>
                            <th scope="row">{index}</th>
                            <td>{q.get('clientName')}</td>
                            <td>{q.get('stage')}</td>
                            <td>{q.get('createdAt')}</td>
                            <td>{q.get('updatedAt')}</td>
                        </tr>
                        )
                    })
                    }
                
                </tbody>
            </Table>
            </div>
            // <div className="listing">
                
            //   {/* <h3>{this.state.data.id ? 'Edit Client' : 'Add New Client'}</h3>
            //   <hr/>
            //   <form className="form form-vertical">
            //     <div className="control-group">
            //       <label>Client Name *</label>
            //       <div className="controls">
            //         <input ref="clientName" type="text" className="form-control" value={this.state.data.clientName} onChange={this.handleDataChange.bind(this, 'clientName')}/>
            //         {this.state.error.name && <small className="text-danger">{this.state.error.name}</small>}
            //       </div>
            //     </div>
            //     <div className="control-group">
            //       <label>Stage *</label>
            //       <div className="controls">
            //         <input ref="stage" type="text" className="form-control" value={this.state.data.stage} onChange={this.handleDataChange.bind(this, 'stage')}/>
            //         {this.state.error.name && <small className="text-danger">{this.state.error.stage}</small>}
            //       </div>
            //     </div>
            //     <div className="control-group">
            //       <label></label>
            //       <div className="controls">
            //         <button type="button" className="btn btn-primary" onClick={this.handleSubmit}>
            //           Save
            //         </button>
            //       </div>
            //     </div>
            //   </form> */}
            // </div>
          );
    }
  }
  
  function mapStateToProps(state) {
      console.log("aasss",state)
    return { clients: state.clients }
  }
  
  export default connect(mapStateToProps, { loadClients })(ClientForm)